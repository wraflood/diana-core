import json
from flask import Blueprint, Response, request

from .message_for_test import message_for_test

logic = Blueprint('logic', __name__)


## Testing API
message_for_test = logic.route('/m')(message_for_test)
