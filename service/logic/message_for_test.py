import json
from flask import abort, request, Response


def message_for_test():
    msg = request.args.get('msg', '')
    uid = request.args.get('uid', '')

    try:
        if type(msg) != str:
            raise TypeError('`msg` is not valid.')

        if not msg:
            raise ValueError('`msg` is required.')

        return Response(
            json.dumps({
                'to': [uid],
                'messages': [
                    {
                        'type': 'text',
                        'text': '收到一則來自 {} 的訊息，內容是：'.format(uid),
                    },
                    {
                        'type': 'text',
                        'text': msg,
                    },
                ],
            }, ensure_ascii=False),
            mimetype='application/json',
            status=200,
        )
    except Exception as e:
        return Response(
            json.dumps({
                'error': str(e),
            }),
            mimetype='application/json',
            status=400,
        )
