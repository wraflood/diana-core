from flask import Flask

from .logic import logic

import json


app = Flask(__name__)
app.config.from_object(__name__)
app.config['JSON_AS_ASCII'] = False

app.register_blueprint(logic, url_prefix='/logic')

@app.route('/', methods=['GET'])
def index():
    return 'Hey hey waz goin on'
